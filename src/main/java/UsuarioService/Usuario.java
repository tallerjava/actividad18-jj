/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UsuarioService;

/**
 *
 * @author Meteoro
 */
class Usuario {

    protected final String username;
    protected final String mail;
    protected final String password;

    public Usuario(String username, String mail, String password) {

        this.username = username;
        this.mail = mail;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Usuario{" + "username=" + username + ", mail=" + mail + ", password=" + password + '}';
    }

}
