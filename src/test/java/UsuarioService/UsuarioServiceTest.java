/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UsuarioService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Meteoro
 */
public class UsuarioServiceTest {

    private UsuarioService usuarioService;

    @Before
    public void setUp() {

        usuarioService = new UsuarioService();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validarUser_valoresValidos_retornaTrue() {

        Usuario usuario = new Usuario("Javier", "javi@gamil.com", "123456p");
        boolean esValido = usuarioService.validarUser(usuario);
        assertTrue(esValido);

    }

    @Test
    public void validarUser_usernameDemasiadoChico_retornaFalse() {
        Usuario usuario = new Usuario("Javi", "javi@gamil.com", "123456p");
        boolean esValido = usuarioService.validarUser(usuario);
        assertFalse(esValido);

    }

    @Test
    public void validarUser_usernameDemasiadoLargo_retornaFalse() {
        Usuario usuario = new Usuario("Javierddddssdddddddd2", "javi@gamil.com", "123456p");
        boolean esValido = usuarioService.validarUser(usuario);
        assertFalse(esValido);
    }

    @Test
    public void validarMail_SimArroba_retornaFalse() {
        Usuario usuario = new Usuario("Javier", "javigamil.com", "123456p");
        boolean esValido = usuarioService.validarUser(usuario);
        assertFalse(esValido);

    }

    @Test
    public void validarMail_SimPunto_retornaFalse() {
        Usuario usuario = new Usuario("Javier", "javi@gamicom", "123456p");
        boolean esValido = usuarioService.validarUser(usuario);
        assertFalse(esValido);
    }

    @Test
    public void validarPass_passwordDemasiadoChico_retornaFalse() {
        Usuario usuario = new Usuario("Javier", "javi@gamil.com", "12345");
        boolean esValido = usuarioService.validarUser(usuario);
        assertFalse(esValido);
    }

    @Test
    public void validarPass_contenerletra_y_numero_retornaFalse() {
        Usuario usuario = new Usuario("Javier", "javi@gamil.com", "mmmmmm");
        boolean esValido = usuarioService.validarUser(usuario);
        assertFalse(esValido);
    }

    @Test
    public void validarPass_Sim_pipe_retornaFalse() {
        Usuario usuario = new Usuario("Javier", "javi@gamil.com", "mmm|mmm1");
        boolean esValido = usuarioService.validarUser(usuario);
        assertFalse(esValido);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
